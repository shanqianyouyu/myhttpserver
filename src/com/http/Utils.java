package com.http;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName: CalculateUrl
 * @Description:
 * @Author: Chase [1131497375@qq.com]
 * @CreateDate: 2021/3/22 0022 21:37
 * @Version: 1.0.0
 */
public class Utils {
    public static String calUrl(String url) {
        String vis = url.substring(1, 5);

        if (vis.equals("add?")) {
            int[] arr = null;
            try {
                arr = dealURL(url);
            } catch (Exception e) {
                return e.getMessage();
            }

            return String.valueOf(arr[0] + arr[1]);
        } else if (vis.equals("mult")) {
            int[] arr = null;
            try {
                arr = dealURL(url);
            } catch (Exception e) {
                return e.getMessage();
            }
            return String.valueOf(arr[0] * arr[1]);
        }
        return "unkonw";
    }

    private static int[] dealURL(String url) throws Exception {
        int[] arr = new int[2];
        int a = 0, b = 0, c = 0, d = 0, m = 0, n = 0;
        for (int i = 0; i < url.length(); i++) {
            if (url.charAt(i) == 'a' && url.charAt(i + 1) == '=') {
                i += 2;
                a = i;
                while (i < url.length()) {
                    if (url.charAt(i) >= '0' && url.charAt(i) <= '9') i++;
                    else break;
                }
                b = i ;
                m = Integer.valueOf(url.substring(a, b));
                if (url.charAt(i) != '&') throw new Exception("wrong format");
            }
//            if (url.charAt(i) != '&') throw new Exception("wrong format");
            if (url.charAt(i) == 'b' && url.charAt(i + 1) == '=') {
                i += 2;
                a = i;
                while (i < url.length()) {
                    if (url.charAt(i) >= '0' && url.charAt(i) <= '9') i++;
                    else break;
                }
                b = i ;
                n = Integer.valueOf(url.substring(a, b));
            }
        }
        arr[0] = m;
        arr[1] = n;
        return arr;
    }
}
