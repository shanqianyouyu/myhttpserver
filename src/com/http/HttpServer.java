package com.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName: HttpServer
 * @Description:
 * @Author: Chase [1131497375@qq.com]
 */
public class HttpServer {
    private int PORT = 80;
    private ServerSocket serverSocket;

    public HttpServer() throws IOException {
        serverSocket = new ServerSocket(PORT);
    }

    public void start() throws IOException {
        while (true) {
            int count = 0;
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();
            while (count == 0) {
                count = inputStream.available();
            }
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            String head = new String(bytes);
            String output = head.split(" ")[1];
//            System.out.println(output);
            output = Utils.calUrl(output);
            OutputStream outputStream = socket.getOutputStream();

            outputStream.write(output.getBytes());

            outputStream.flush();
            outputStream.close();
            socket.close();
        }
    }

    public static void main(String[] args) throws IOException {
        new HttpServer().start();

    }
}
